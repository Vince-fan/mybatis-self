package com.lagou;

import com.lagou.pojo.User;

import java.util.List;

public interface UserDao {

    public List<User> findall();  //查询所有

    public User findByCondition();
}

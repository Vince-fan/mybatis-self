package com.lagou.pojo;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

/**
 * 全局配置类
 * 存放数据库信息，以及查询SQL的相关信息
 */
public class Configuration {

    //存放数据库的信息
    private DataSource dataSource;

    /**
     * 存放mapper.xml中的SQL信息，因为一个Mapper.xml中有写的多个SQL，所有用Map
     * key: namespace + "." + id
     */
    private Map<String,MappedStatement> mappedStatementMap = new HashMap<>();

    public DataSource getDataSource() {
        return dataSource;
    }

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public Map<String, MappedStatement> getMappedStatementMap() {
        return mappedStatementMap;
    }

    public void setMappedStatementMap(Map<String, MappedStatement> mappedStatementMap) {
        this.mappedStatementMap = mappedStatementMap;
    }

}

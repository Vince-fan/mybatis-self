package com.lagou.config;

import com.lagou.pojo.Configuration;
import com.lagou.io.Resources;
import com.lagou.pojo.Configuration;
import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.io.InputStream;
import java.util.List;
import java.util.Properties;

import java.io.InputStream;

public class XMLConfigBuilder {

    private Configuration configuration;

    //创建无参构造，new XMLConfigBuilder()的时候就创建了configuration
    public XMLConfigBuilder(){
        this.configuration = new Configuration();
    }

    /**
     * 该方法就是使用dom4j将配置文件中的信息解析，封装在configuration对象中
     * @param inputStream
     */
    public Configuration parse(InputStream inputStream) throws Exception {

        Document document = new SAXReader().read(inputStream);

        //获取跟根标签<configuration>
        Element rootElement = document.getRootElement();
    //解析数据源
        List<Element> propertyList = rootElement.selectNodes("//property");

        //用于存储key-values形式的容器，跟map类似
        Properties properties = new Properties();
        for (Element element : propertyList) {
            String name = element.attributeValue("name");
            String value = element.attributeValue("value");
            properties.put(name,value);
        }

        //创建连接池
        ComboPooledDataSource comboPooledDataSource = new ComboPooledDataSource();
        comboPooledDataSource.setDriverClass(properties.getProperty("driverClass"));
        comboPooledDataSource.setJdbcUrl(properties.getProperty("jdbcUrl"));
        comboPooledDataSource.setUser(properties.getProperty("user"));
        comboPooledDataSource.setPassword(properties.getProperty("password"));

        //设计到configuration中
        configuration.setDataSource(comboPooledDataSource);

    //mapper.xml解析：拿到路劲--字节流输入--dom4j进行解析
        List<Element> mapperList = document.selectNodes("//mapper");

        for (Element element : mapperList) {

            //获取mapper.xml文件地址
            String resource = element.attributeValue("resource");
            //创建工具类进行解析
            XMLMapperBuilder xmlMapperBuilder = new XMLMapperBuilder(configuration);
            InputStream resourcesAsStream = Resources.getResourcesAsStream(resource);

            xmlMapperBuilder.parse(resourcesAsStream);
        }

        return configuration;
    }
}

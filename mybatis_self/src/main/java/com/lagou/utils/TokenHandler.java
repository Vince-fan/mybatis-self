package com.lagou.utils;

/**
 * 标记处理类：配置标记解析器来完成对占位符的解析处理工作
 * 实现类是 ParameterMappingTokenHandler
 */
public interface TokenHandler {

    String handleToken(String content);

}

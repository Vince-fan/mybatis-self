package com.lagou.sqlSession;

import com.lagou.pojo.Configuration;
import com.lagou.pojo.MappedStatement;

import java.lang.reflect.*;
import java.util.List;

public class DefaultSqlSession implements SqlSession{

    private Configuration configuration;

    public DefaultSqlSession(Configuration configuration) {
        this.configuration = configuration;
    }

    @Override
    public <T> List<T> selectList(String statementId, Object... params) throws Exception {
        //创建执行器
        SimpleExecutor simpleExecutor = new SimpleExecutor();
        MappedStatement mappedStatement = configuration.getMappedStatementMap().get(statementId);
        List<Object> query = simpleExecutor.query(configuration, mappedStatement, params);
        return (List<T>) query;
    }

    @Override
    public <T> T selectOne(String statementId, Object... params) throws Exception {
        //直接调用上面的方法就行
        List<Object> list = selectList(statementId, params);

        if(list.size() == 1){
            return (T) list.get(0);
        }else {
            throw new RuntimeException("查询结果为空，或者返回值过多");
        }
    }

    @Override
    public <T> T getMapper(Class<?> mapperClass) throws Exception {

        Object proxyInstance = Proxy.newProxyInstance(DefaultSqlSession.class.getClassLoader(), new Class[]{mapperClass}, new InvocationHandler() {
            @Override
            public Object invoke(Object o, Method method, Object[] args) throws Throwable {

                String methodName = method.getName();
                String className = method.getDeclaringClass().getName();

                String statementId = className + "." +methodName;

                if (methodName.toLowerCase().contains("insert")){
                    // 走insert 方法
                }

                if (methodName.toLowerCase().contains("delete")){
                    // 走delete方法
                }

                if (methodName.toLowerCase().contains("update")){
                    // 走update方法
                }

                Type genericReturnType = method.getGenericReturnType();

                //判断是否进行了泛型类型参数化，如果是泛型了，说明用的是list
                if (genericReturnType instanceof ParameterizedType){
                    List<Object> objects = selectList(statementId, args);
                    return objects;
                }

                return selectOne(statementId, args);
            }
        });
        return (T) proxyInstance;
    }
}

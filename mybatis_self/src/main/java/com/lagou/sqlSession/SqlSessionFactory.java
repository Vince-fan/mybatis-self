package com.lagou.sqlSession;

/**
 * 就是获取SqlSessio对象，实现类是DefaultSqlSessionFactory
 */
public interface SqlSessionFactory {

    public SqlSession openSession();

}

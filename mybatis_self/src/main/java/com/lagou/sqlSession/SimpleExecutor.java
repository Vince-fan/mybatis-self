package com.lagou.sqlSession;

import com.lagou.config.BoundSql;
import com.lagou.pojo.Configuration;
import com.lagou.pojo.MappedStatement;
import com.lagou.utils.GenericTokenParser;
import com.lagou.utils.ParameterMapping;
import com.lagou.utils.ParameterMappingTokenHandler;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class SimpleExecutor implements Executor{

    @Override
    public <E> List<E> query(Configuration configuration, MappedStatement mappedStatement, Object... params) throws SQLException, Exception {

        //1、得到链接
        Connection connection = configuration.getDataSource().getConnection();

        /**
         * 获取  select * from simpleuser where id = #{id} and username = #{username}
         * 2、转换sql select * from simpleuser where id = ? and username = ?  ,转还换过程中还需要#{}中的值进行解析，存储
         */
        String sqlText = mappedStatement.getSqlText();
        BoundSql boundSql = getBoundSql(sqlText);

        //3、设置预处理对象
        PreparedStatement preparedStatement = connection.prepareStatement(boundSql.getSqlText());

        //4、设置参数
        String parameterType = mappedStatement.getParameterType();  //获取入参
        Class<?> parameterTypeClass = getParameterTypeClass(parameterType);  //通过反射获取入参的类型
        List<ParameterMapping> parameterMappingList = boundSql.getParameterMappingList();

        for (int i = 0; i < parameterMappingList.size(); i++) {

            ParameterMapping parameterMapping = parameterMappingList.get(i);
            String content = parameterMapping.getContent();  //就是#{id}中的id
            Field declaredField = parameterTypeClass.getDeclaredField(content);

            //暴力访问，防止类的属性是private属性
            declaredField.setAccessible(true);

            Object o = declaredField.get(params[0]);

            preparedStatement.setObject(i+1,o);

        }

        //5、执行sql
        ResultSet resultSet = preparedStatement.executeQuery();

        //6、封装返回集
        String resultType = mappedStatement.getResultType();
        Class<?> resultTypeClass = getParameterTypeClass(resultType);

        ArrayList<Object> returnList = new ArrayList<>();

        while (resultSet.next()){

            Object o = resultTypeClass.newInstance();

            ResultSetMetaData metaData = resultSet.getMetaData();
            for (int i = 1; i <= metaData.getColumnCount(); i++) {

                String columnName = metaData.getColumnName(i);
                Object object = resultSet.getObject(columnName);

                PropertyDescriptor propertyDescriptor = new PropertyDescriptor(columnName,resultTypeClass);
                Method writeMethod = propertyDescriptor.getWriteMethod();
                writeMethod.invoke(o,object);
            }
            returnList.add(o);
        }
        return (List<E>) returnList;
    }

    /**
     * 根据xml中的select中设计的类的全路径获（parameterType=""）取类的相关信息
     * @param parameterType
     */
    private  Class<?> getParameterTypeClass(String parameterType) throws Exception {
        if(parameterType == null){
            throw new RuntimeException("parameterType is not null");
        }
        return Class.forName(parameterType);
    }

    /**
     * 完成对#{}的解析工作：1.将#{}使用?进行代替；2.解析出#{}中的值进行存储
     * @param sqlText :sql语句
     * @return
     */
    private BoundSql getBoundSql(String sqlText) {

        ParameterMappingTokenHandler parameterMappingTokenHandler = new ParameterMappingTokenHandler();
        GenericTokenParser genericTokenParser = new GenericTokenParser("#{", "}", parameterMappingTokenHandler);
        //解析出来的SQL
        String parseSql = genericTokenParser.parse(sqlText);
        //#{}解析出来的参数名
        List<ParameterMapping> parameterMappings = parameterMappingTokenHandler.getParameterMappings();
        //标记处理类：配置标记解析器来完成对占位符的解析处理工作
        BoundSql boundSql = new BoundSql(parseSql, parameterMappings);

        return boundSql;
    }
}

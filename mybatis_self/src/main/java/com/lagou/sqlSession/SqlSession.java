package com.lagou.sqlSession;

import java.util.List;

/**
 *
 * 会话类，封装的增删改查方法，实现类是DefaultSqlSession
 */
public interface SqlSession {

    public <T> T selectOne(String statementId,Object ... params) throws Exception;

    public <T> List<T> selectList(String statementId, Object ... params) throws Exception;

    public <T> T getMapper(Class<?> mapperClass) throws Exception;

}
